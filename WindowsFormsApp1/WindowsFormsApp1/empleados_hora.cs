﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class empleados_hora : Form
    {
        public empleados_hora()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            double horas, res, saldo, resultado;

            horas = double.Parse(pantalla_hora2.Text);
            saldo = double.Parse(pantalla_hora1.Text);

            if (horas <= 8)
            {
                resultado = (horas * saldo);
                MessageBox.Show(" El Salario del Empleado por Hora es: " + resultado + " $");
                MessageBox.Show(" Gracias, Vuelva Pronto ");
            }
            else
            {
                resultado = (horas * saldo);
                res = (resultado * 1.5);
                MessageBox.Show(" El Salario del Empleado por Hora es: " + res + " $");
                MessageBox.Show(" Gracias, Vuelva Pronto ");
            }
            
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
