﻿namespace WindowsFormsApp1
{
    partial class empleados_hora
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.pantalla_hora2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pantalla_hora1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(100, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "EMPLEADOS POR HORA";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(158, 182);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "Calcular Sueldo";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // pantalla_hora2
            // 
            this.pantalla_hora2.Location = new System.Drawing.Point(194, 125);
            this.pantalla_hora2.Name = "pantalla_hora2";
            this.pantalla_hora2.Size = new System.Drawing.Size(100, 20);
            this.pantalla_hora2.TabIndex = 9;
            this.pantalla_hora2.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(69, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Horas Trabajadas";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // pantalla_hora1
            // 
            this.pantalla_hora1.Location = new System.Drawing.Point(194, 64);
            this.pantalla_hora1.Name = "pantalla_hora1";
            this.pantalla_hora1.Size = new System.Drawing.Size(100, 20);
            this.pantalla_hora1.TabIndex = 7;
            this.pantalla_hora1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(66, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Sueldo Por Hora";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // empleados_hora
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(377, 411);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.pantalla_hora2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pantalla_hora1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "empleados_hora";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox pantalla_hora2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox pantalla_hora1;
        private System.Windows.Forms.Label label2;
    }
}