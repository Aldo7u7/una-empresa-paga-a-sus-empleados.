﻿namespace WindowsFormsApp1
{
    partial class empleados_comision
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.pantalla_comosion1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pantalla_comosion2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(79, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "**EMPLEADOS POR COMSION**";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(130, 163);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 15;
            this.button2.Text = "Calcular Sueldo";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // pantalla_comosion1
            // 
            this.pantalla_comosion1.Location = new System.Drawing.Point(177, 58);
            this.pantalla_comosion1.Name = "pantalla_comosion1";
            this.pantalla_comosion1.Size = new System.Drawing.Size(100, 20);
            this.pantalla_comosion1.TabIndex = 14;
            this.pantalla_comosion1.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(64, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Horas Trabajadas";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(41, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Ventas Brutas Mensuales";
            // 
            // pantalla_comosion2
            // 
            this.pantalla_comosion2.Location = new System.Drawing.Point(177, 113);
            this.pantalla_comosion2.Name = "pantalla_comosion2";
            this.pantalla_comosion2.Size = new System.Drawing.Size(100, 20);
            this.pantalla_comosion2.TabIndex = 17;
            // 
            // empleados_comision
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 369);
            this.Controls.Add(this.pantalla_comosion2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.pantalla_comosion1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Name = "empleados_comision";
            this.Text = "**empleados_comision**";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox pantalla_comosion1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox pantalla_comosion2;
    }
}