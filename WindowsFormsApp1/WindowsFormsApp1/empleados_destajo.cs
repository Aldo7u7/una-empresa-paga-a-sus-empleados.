﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class empleados_destajo : Form
    {
        public empleados_destajo()
        {
            InitializeComponent(); // a
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double cant_piezas, precio_piezas, resultado;

            cant_piezas = double.Parse(textBox1.Text);
            precio_piezas = double.Parse(textBox2.Text);
            resultado = (cant_piezas * precio_piezas);
            MessageBox.Show(" El Salario del Empleado de Destajo es: " + resultado + " $");
            MessageBox.Show(" Gracias, Vuelva Pronto ");

        }
    }
}
