﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class empleados_comision : Form
    {
        public empleados_comision()
        {
            InitializeComponent();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            double res, total, ventas, resultado, horas;

            horas = double.Parse(pantalla_comosion1.Text);
            ventas = float.Parse(pantalla_comosion2.Text);
            res = (horas * 250);
            total = (ventas * 0.057);
            resultado = (res + total);

            MessageBox.Show(" El Salario NETO del Empleado por Comision es: " + res + " $");
            MessageBox.Show(" El Salario TOTAL del Empleado por Comision es: " + resultado + " $");
            MessageBox.Show(" Gracias, Vuelva Pronto ");


        }
    }
}
